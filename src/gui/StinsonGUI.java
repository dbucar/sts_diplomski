package gui;

import runner.Starter;

import java.awt.EventQueue;

import javax.swing.JFrame;

import java.awt.GridLayout;

import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.BorderLayout;

import javax.swing.JToolBar;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JButton;
import javax.swing.SwingWorker;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class StinsonGUI implements ActionListener {

	private JFrame frame;

	JPanel headerPanel;
	JToolBar paramToolBar;
	JPanel paramPanel;
	JLabel lblVertices;
	JSpinner spinnerVertices;
	JButton btnIzracunaj;
	JTextArea textAreaResult;
	JScrollPane resultPanel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StinsonGUI window = new StinsonGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StinsonGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		headerPanel = new JPanel();
		frame.getContentPane().add(headerPanel, BorderLayout.NORTH);

		paramToolBar = new JToolBar();
		paramToolBar.setFloatable(false);
		headerPanel.add(paramToolBar);

		paramPanel = new JPanel();
		paramToolBar.add(paramPanel);

		lblVertices = new JLabel("Broj to�aka");
		paramPanel.add(lblVertices);

		spinnerVertices = new JSpinner();
		spinnerVertices.setModel(new SpinnerNumberModel(7, 7, 5200, 6));
		paramPanel.add(spinnerVertices);

		btnIzracunaj = new JButton("Izra�unaj");
		btnIzracunaj.addActionListener(this);
		headerPanel.add(btnIzracunaj);

		textAreaResult = new JTextArea();
		textAreaResult.setColumns(4);
		textAreaResult.setTabSize(4);

		resultPanel = new JScrollPane(textAreaResult);
		frame.getContentPane().add(resultPanel);
		frame.getContentPane().setFocusTraversalPolicy(
				new FocusTraversalOnArray(new Component[] { headerPanel,
						resultPanel, textAreaResult }));

	}

	public void actionPerformed(ActionEvent e) {

		// disable gui
		btnIzracunaj.setEnabled(false);
		spinnerVertices.setEnabled(false);
		textAreaResult.setText(" Molim pri�ekajte...");

		StinsonRunner sr = new StinsonRunner(spinnerVertices.getValue());
		sr.execute();

	}

	class StinsonRunner extends SwingWorker<Void, Void> {

		int v;
		String strResult;

		public StinsonRunner(Object numV) {
			v = (Integer) numV;
		}

		@Override
		protected Void doInBackground() throws Exception {

			if (!(v % 6 == 1 || v % 6 == 3)) {
				strResult = " Mora vrijediti v = 1,3 mod 6!";
			} else {
				Starter s = new Starter(v, 2, true);
				s.start();
				strResult = " Blokovi SST(" + v + "):\n" + s.blocksToString()
						+ "\n Broj iteracija: " + s.getNumIter()
						+ "\n Vrijeme izvr�avanja: " + s.getTime() + "ms";
			}

			return null;
		}

		/*
		 * Executed in event dispatching thread
		 */
		@Override
		public void done() {
			textAreaResult.setText(strResult);

			// enable gui
			btnIzracunaj.setEnabled(true);
			spinnerVertices.setEnabled(true);
		}

	}

}
