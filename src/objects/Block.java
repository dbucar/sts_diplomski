package objects;

public class Block {

	public int a;
	public int b;
	public int c;

	public Block(int x, int y, int z) {
		
		// sort elements - 3 comparisons needed
		int temp;
		if (x > y) {
			temp = x;
			x = y;
			y = temp;
		}
		if (y > z) {
			temp = y;
			y = z;
			z = temp;
		}
		if (x > y) {
			temp = x;
			x = y;
			y = temp;
		}
		
		// assign in ascending order
		a = x;
		b = y;
		c = z;
	}

	@Override
	public boolean equals(Object o) {
		Block b = (Block) o;
		if (this.a == b.a && this.b == b.b && this.c == b.c)
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		return a * 1013 + b * 673 + c * 13;
	}
	
	public String toString(){
		return String.format("(%d, %d, %d)", a, b, c);
	}

}
