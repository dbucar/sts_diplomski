package objects;

public class Pair {

	public int a;
	public int b;

	public Pair(int x, int y) {

		// sort in ascending order
		int temp;
		if (x > y) {
			temp = x;
			x = y;
			y = temp;
		}

		a = x;
		b = y;
	}

	@Override
	public boolean equals(Object obj) {
		Pair p = (Pair) obj;
		if (this.a == p.a && this.b == p.b)
			return true;
		return false;
	}

	@Override
	public int hashCode() {
		return a * 673 + b * 13;
	}
	
	public String toString(){
		return String.format("(%d, %d)", a, b);
	}

}
