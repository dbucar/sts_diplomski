package runner;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import objects.Block;

import org.apache.log4j.Logger;

import stinson.*;

public class Starter {

	private int vSize;
	private int algoType;
	private boolean verbose;

	private Set<Block> blocks;
	private long runTime;
	private int numIter;

	public Starter(int vSize, int algoType, boolean verbose) {
		this.vSize = vSize;
		this.algoType = algoType;
		this.verbose = verbose;
	}

	public void start() {

		Logger log = Logger.getLogger(Starter.class.getName());

		int bSize = (vSize * (vSize - 1)) / 6;

		System.out.println("v = " + vSize + "; b = " + bSize);
		// log.info("v = " + vSize + "; b = " + bSize);

		Stinson s = null;
		try {
			switch (algoType) {
			case 0: // classic
				// log.info("Stinson Prime (Custom DS)");
				s = new StinsonPrime(vSize, bSize, false, verbose);
				break;
			case 1: // greedy
				// log.info("Stinson Prime Greedy (Custom DS)");
				s = new StinsonPrime(vSize, bSize, true, verbose);
				break;
			case 2: // book data structures
				// log.info("Stinson Revised (Book DS)");
				s = new StinsonRevised(vSize, bSize, verbose);
			}

			long time = System.currentTimeMillis();
			blocks = s.run();
			runTime = (System.currentTimeMillis() - time);
			numIter = s.getNumIter();
			System.out.println("Total time: " + runTime + "ms");
			// log.info("v = " + vSize + "; b = " + bSize + "; Total time = "
			// + (System.currentTimeMillis() - time) + "ms; Iterations = "
			// + numIter);

			// printBlocks(blocks);
			// printBlocksToLog(log, blocks);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int getNumIter() {
		return numIter;
	}

	public long getTime() {
		return runTime;
	}

	private static void printBlocks(Set<Block> blocks) {
		int i = 1;
		System.out.print(i + ":\t");
		for (Block b : blocks) {
			System.out.print(b + "\t");
			if (i++ % 3 == 0)
				System.out.print("\n" + i + ":\t");
		}

	}

	private static void printBlocksToLog(Logger log, Set<Block> blocks) {
		int i = 1;
		StringBuilder sb = new StringBuilder();
		sb.append(i + ":\t");
		for (Block b : blocks) {
			sb.append(b + "\t");
			if (i++ % 3 == 0) {
				log.info(sb.toString());
				sb.setLength(0);
				sb.append(i + ":\t");
			}
		}

	}

	public String blocksToString() {

		int i = 1;
		StringBuilder sb = new StringBuilder();
		sb.append(" " + i + ":\t");
		for (Iterator<Block> iter = blocks.iterator(); iter.hasNext();) {
			sb.append(iter.next() + "\t");
			if (i++ % 3 == 0) {
				sb.append("\n");
				if (iter.hasNext()) {
					sb.append(" " + i + ":\t");
				}
			}
		}
//		for (Block b : blocks) {
//			sb.append(b + "\t");
//			if (i++ % 3 == 0) {
//				sb.append("\n");
//				sb.append(i + ":\t");
//			}
//		}

		return sb.toString();
	}

}
