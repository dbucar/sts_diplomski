package stinson;

import java.util.Set;

import objects.Block;

public interface Stinson {

	public Set<Block> run();

	public int getNumIter();

}
