package stinson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import objects.Block;
import objects.Pair;

import org.apache.log4j.Logger;

public class StinsonPrime implements Stinson {

	private int vSize;
	private int bSize;
	private int blocksWithX;
	private Map<Pair, Block> pairOccursIn;
	private int[] occurence; // initialized to 0
	public Set<Block> blocks;
	private boolean greed;
	private static Logger log;
	private boolean verbose;

	private int numIter;

	public StinsonPrime(int vSize, int bSize, boolean greed, boolean verbose)
			throws IOException {
		this.vSize = vSize;
		this.bSize = bSize;
		blocks = new HashSet<>();
		occurence = new int[vSize];
		blocksWithX = (vSize - 1) / 2;
		pairOccursIn = new HashMap<>();
		// initPairOccursIn();
		this.greed = greed;
		log = Logger.getLogger(StinsonPrime.class.getName());
		this.verbose = verbose;
		numIter = 0;
	}

	public Set<Block> run() {
		// run the construction heuristic
		while (blocks.size() < bSize) {
			switchHeuristic();
		}
		return blocks;
	}

	private void switchHeuristic() {

		// System.out.println("SWITCH");

		int x = getLivePoint();
		if (x == -1) {
			System.err.println("No more live points!");
			System.exit(-2);
		}

		for (int y = 0; y < vSize; ++y) {
			for (int z = y + 1; z < vSize; ++z) {
				if (x != y && x != z) {
					if (isLivePair(x, y) && isLivePair(x, z)) {
						if (isLivePair(y, z)) { // {y,z} is a live pair s.t.
												// {x,y}
												// and {x,z} are live pairs
							// addNewBlock(x, y, z);
						} else {
							Block oldYZblock = (pairOccursIn
									.get(new Pair(y, z)));
							if (oldYZblock != null)
								removeBlock(oldYZblock);
						}
						addNewBlock(x, y, z);
						numIter++;
						if (!greed)
							return; // original algorithm has this included
					}
				}
			}
		}

	}

	/**
	 * Remove block bl from set of all blocks and map pairOccursIn, update point
	 * occurence counters
	 * 
	 * @param bl
	 */
	private void removeBlock(Block bl) {

		blocks.remove(bl);
		int x = bl.a;
		int y = bl.b;
		int z = bl.c;

		Pair p = new Pair(x, y);
		pairOccursIn.remove(p);

		p = new Pair(x, z);
		pairOccursIn.remove(p);

		p = new Pair(y, z);
		pairOccursIn.remove(p);

		// update point occurence counters
		occurence[x]--;
		occurence[y]--;
		occurence[z]--;

		// System.out.println(String.format("Removing block %s\t(Blocks: %d/%d)",
		// bl, blocks.size(), bSize));
		if (verbose)
			log.info(String.format("Removing block %s\t(Blocks: %d/%d)", bl,
					blocks.size(), bSize));

	}

	private void addNewBlock(int x, int y, int z) {

		Block bl = new Block(x, y, z);

		blocks.add(bl);

		// update pair occurences
		pairOccursIn.put(new Pair(x, y), bl);
		pairOccursIn.put(new Pair(x, z), bl);
		pairOccursIn.put(new Pair(y, z), bl);

		// update point occurence counters
		occurence[x]++;
		occurence[y]++;
		occurence[z]++;

		// System.out.println(String.format("Adding block %s\t(Blocks: %d/%d)",
		// bl, blocks.size(), bSize));
		if (verbose)
			log.info(String.format("Adding block %s\t(Blocks: %d/%d)", bl,
					blocks.size(), bSize));
	}

	/**
	 * Randomized choice of a live point. Essential for algorithm termination.
	 * 
	 * @return live point if one exists, -1 otherwise
	 */
	private int getLivePoint() {

		// "any" live point --> randomize the choice
		Random rand = new Random();
		int cnt = 0;
		for (int i = rand.nextInt(vSize); cnt < vSize; i = (i + 1) % vSize, cnt++) {
			if (occurence[i] < blocksWithX) {
				// System.out.println("Live point: " + i);
				if (verbose)
					log.info("Live point: " + i);
				return i;
			}
		}
		return -1;
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @return true if pair does not occur in any block
	 */
	private boolean isLivePair(int x, int y) {
		if (pairOccursIn.get(new Pair(x, y)) == null) {
			// System.out.println("Live pair: (" + x + ", " + y + ")");
			if (verbose)
				log.info("Live pair: (" + x + ", " + y + ")");
			return true;
		}
		return false;
	}

	@Override
	public int getNumIter() {
		return numIter;
	}
}
