package stinson;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Logger;

import objects.Block;

public class StinsonRevised implements Stinson {

	private int vSize;
	private int bSize;
	private boolean verbose;
	private static Logger log;

	private int numLivePoints;
	private int[] livePoints;
	private int[] indexLivePoints;
	private int[] numLivePairs;
	private int[][] livePairs;
	private int[][] indexLivePairs;
	private int[][] other;

	private int numBlocks;
	private Random rand;

	private int numIter;

	public StinsonRevised(int vSize, int bSize, boolean verbose) {

		this.vSize = vSize;
		this.bSize = bSize;
		this.verbose = verbose;
		rand = new Random();
		log = Logger.getLogger(StinsonPrime.class.getName());

		livePoints = new int[vSize + 1];
		indexLivePoints = new int[vSize + 1];
		numLivePairs = new int[vSize + 1];
		livePairs = new int[vSize + 1][vSize + 1];
		indexLivePairs = new int[vSize + 1][vSize + 1];
		other = new int[vSize + 1][vSize + 1];
		numIter = 0;
	}

	@Override
	public Set<Block> run() {

		// algorithm steps
		numBlocks = 0;
		initialize(vSize);
		while (numBlocks < bSize) {
			revisedSwitch();
			numIter++;
		}
		Set<Block> blocks = constructBlocks(vSize, other);
		return blocks;

	}

	/***
	 * Initialize arrays
	 * 
	 * @param vSize
	 */
	private void initialize(int vSize) {

		numLivePoints = vSize;

		// elements from 1 to v
		for (int x = 1; x <= vSize; x++) {
			livePoints[x] = x;
			indexLivePoints[x] = x;
			numLivePairs[x] = vSize - 1;
			for (int y = 1; y <= vSize - 1; y++) {
				livePairs[x][y] = (y + x - 1 + vSize) % vSize + 1; // + vSize
																	// added
																	// because
																	// of the
																	// negative
																	// sign
			}
			for (int y = 1; y <= vSize; y++) {
				indexLivePairs[x][y] = (y - x + vSize) % vSize; // + vSize added
																// because of
																// the negative
																// sign
				other[x][y] = 0;
			}

		}

	}

	private void revisedSwitch() {

		// r random integer, 1 <= r <= numLivePoints
		int r = rand.nextInt(numLivePoints) + 1;
		int x = livePoints[r];

		// s,t random integers, 1 <= s < t <= numLivePairs[x]
		int s = rand.nextInt(numLivePairs[x]) + 1;
		int t;
		if (s == numLivePairs[x]) {
			s -= 1;
			t = numLivePairs[x];
		} else {
			t = rand.nextInt(numLivePairs[x] - s) + 1 + s;
		}

		int y = livePairs[x][s];
		int z = livePairs[x][t];

		int w = other[y][z];
		if (w == 0) {
			addBlock(x, y, z);
			numBlocks += 1;
		} else {
			exchangeBlock(x, y, z, w);
		}

	}

	/**
	 * Create new block {x,y,z}
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	private void addBlock(int x, int y, int z) {

		// System.out.println(String.format("Adding block (%d, %d, %d)", x, y,
		// z));
		if (verbose)
			log.info(String.format("Adding block (%d, %d, %d)", x, y, z));

		// assign block {x,y,z}
		other[x][y] = z;
		other[y][x] = z;
		other[x][z] = y;
		other[z][x] = y;
		other[y][z] = x;
		other[z][y] = x;

		// delete live pairs
		deletePair(x, y);
		deletePair(y, x);
		deletePair(x, z);
		deletePair(z, x);
		deletePair(y, z);
		deletePair(z, y);
	}

	/**
	 * Join x instead of w with pair {y,z}.
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param w
	 */
	private void exchangeBlock(int x, int y, int z, int w) {

		// System.out.println(String.format(
		// "Exchanging block (%d, %d, %d) for (%d, %d, %d)", w, y, z, x,
		// y, z));

		if (verbose)
			log.info(String.format(
					"Exchanging block (%d, %d, %d) for (%d, %d, %d)", w, y, z,
					x, y, z));

		// assign block {x,y,z}
		other[x][y] = z;
		other[y][x] = z;
		other[x][z] = y;
		other[z][x] = y;
		other[y][z] = x;
		other[z][y] = x;

		// dismiss block {w,y,z}
		other[w][y] = 0;
		other[y][w] = 0;
		other[w][z] = 0;
		other[z][w] = 0;

		// revive pairs of w
		insertPair(w, y);
		insertPair(y, w);
		insertPair(w, z);
		insertPair(z, w);

		// delete live pairs of x
		deletePair(x, y);
		deletePair(y, x);
		deletePair(x, z);
		deletePair(z, x);
	}

	/**
	 * Revive pair {x,y}
	 * 
	 * @param x
	 * @param y
	 */
	private void insertPair(int x, int y) {

		if (numLivePairs[x] == 0) {
			numLivePoints += 1;
			livePoints[numLivePoints] = x;
			indexLivePoints[x] = numLivePoints;
		}

		numLivePairs[x] += 1;
		int pos = numLivePairs[x];
		livePairs[x][pos] = y;
		indexLivePairs[x][y] = pos;
	}

	/***
	 * Delete live pair {x,y}
	 * 
	 * @param x
	 * @param y
	 */
	private void deletePair(int x, int y) {

		// current position of y as x's live pair member
		int pos = indexLivePairs[x][y];
		// last position in x's live pairs
		int num = numLivePairs[x];
		// last element of x's live pairs
		int z = livePairs[x][num];

		// put the last to current position == delete y
		livePairs[x][pos] = z;
		indexLivePairs[x][z] = pos;
		livePairs[x][num] = 0;
		indexLivePairs[x][y] = 0;
		numLivePairs[x] -= 1;

		if (numLivePairs[x] == 0) {
			pos = indexLivePoints[x];
			z = livePoints[numLivePoints];
			livePoints[pos] = z;
			indexLivePoints[z] = pos;
			livePoints[numLivePoints] = 0;
			indexLivePoints[x] = 0;
			numLivePoints -= 1;
		}
	}

	/***
	 * Construct blocks B from the <it>Other</it> array
	 * 
	 * @param vSize
	 * @param other
	 * @return
	 */
	private Set<Block> constructBlocks(int vSize, int[][] other) {

		Set<Block> blocks = new HashSet<Block>();

		for (int x = 1; x <= vSize; x++) {
			for (int y = x + 1; y <= vSize; y++) {
				int z = other[x][y];
				if (z > y) {
					blocks.add(new Block(x, y, z));
				}
			}
		}

		return blocks;
	}

	public int getNumIter() {
		return numIter;
	}
}
