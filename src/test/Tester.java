package test;

import org.apache.log4j.Logger;

import runner.Starter;

public class Tester {

	private static long minIter, maxIter, totalIter, minTime, maxTime,
			totalTime;
	private static double avgIter, avgTime;

	private static Logger log = Logger.getLogger(Tester.class.getName());

	public static long getMinIter() {
		return minIter;
	}

	public static long getMaxIter() {
		return maxIter;
	}

	public static double getAvgIter() {
		return avgIter;
	}

	public static void runTests(int maxV, int numRuns, int algoType,
			boolean verbose) {

		int vSize;
		Starter st;

		for (int i = 30; i < maxV; i += 30) {
			vSize = i + 1; // vSize = 1 mod 6
			int bSize = (vSize * (vSize - 1)) / 6;
			long iter;
			long time;
			minIter = Long.MAX_VALUE;
			maxIter = Long.MIN_VALUE;
			totalIter = 0;
			minTime = Long.MAX_VALUE;
			maxTime = Long.MIN_VALUE;
			totalTime = 0;
			for (int j = 0; j < numRuns; j++) {
				st = new Starter(vSize, algoType, verbose);
				st.start();
				// time stats
				time = st.getTime();
				if (time < minTime) {
					minTime = time;
				}
				if (time > maxTime) {
					maxTime = time;
				}
				totalTime += time;
				// iteration stats
				iter = st.getNumIter();
				if (iter < minIter) {
					minIter = iter;
				}
				if (iter > maxIter) {
					maxIter = iter;
				}
				totalIter += iter;
			}
			avgIter = totalIter / (double) numRuns;
			avgTime = totalTime / (double) numRuns;
			log.info(vSize + ";" + bSize + ";" + minIter + ";" + maxIter + ";"
					+ avgIter + ";" + minTime + ";" + maxTime + ";" + avgTime);
			// vSize += 2; // vSize = 3 mod 6
			// st = new Starter(vSize, algoType, verbose);
			// st.start();
		}
	}

}
